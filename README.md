# Pre-requisites
Install docker desktop, follow instustions: https://docs.docker.com/desktop/

# Build Docker Container
docker build . -t kk/artists

# Run container
docker run kk/artists