const axios = require('axios');
const cheerio = require('cheerio');
const url = "https://www.billboard.com/charts/hot-100/";

async function fetchData(url) {
    let response = await axios(url).catch((err) => console.log(err));
    if (response.status !== 200) {
        console.log("Error occurred while fetching data");
        return;
    }
    return response;
}
async function getArtistTitles(url) {
    const res = await fetchData(url);
    const html = res.data;
    const $ = cheerio.load(html);
    const selector = "div.o-chart-results-list-row-container > ul > li.lrv-u-width-100p > ul > li:nth-child(1)";
    const list = $(selector);
    const tracksTitles = [];
    list.each(function () {
        let trackTitle = $(this).find('h3.c-title').text();
        let artistTitle = $(this).find('span.c-label').text();
        tracksTitles.push({ "trackTitle": trackTitle.trim(), "artistTitle": artistTitle.trim() });
    });
    return tracksTitles
        .sort((t1, t2) => t2["trackTitle"].length - t1["trackTitle"].length)
        .map(trackTitle => trackTitle["artistTitle"]);
}

getArtistTitles(url).then(
   artists => {
    artists.forEach(artist => console.log(artist));
});