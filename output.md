$ docker build . -t kk/artists
[+] Building 4.7s (10/10) FINISHED
 => [internal] load build definition from Dockerfile                                                               0.0s
 => => transferring dockerfile: 37B                                                                                0.0s
 => [internal] load .dockerignore                                                                                  0.0s
 => => transferring context: 34B                                                                                   0.0s
 => [internal] load metadata for docker.io/library/node:12                                                         1.8s
 => [1/5] FROM docker.io/library/node:12@sha256:01627afeb110b3054ba4a1405541ca095c8bfca1cb6f2be9479c767a2711879e   0.0s
 => [internal] load build context                                                                                  0.0s
 => => transferring context: 1.30kB                                                                                0.0s
 => CACHED [2/5] WORKDIR /usr/src/app                                                                              0.0s
 => CACHED [3/5] COPY package*.json ./                                                                             0.0s
 => [4/5] COPY *.js ./                                                                                             0.1s
 => [5/5] RUN npm install                                                                                          2.4s
 => exporting to image                                                                                             0.2s
 => => exporting layers                                                                                            0.1s
 => => writing image sha256:10a68032e731fb988afc3f40506f3d4b7fab950b22f52eed60e306e346e0bdbc                       0.0s
 => => naming to docker.io/kk/artists                                                                              0.0s

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them
$
$ docker run kk/artists
Lil Nas X
Taylor Swift
Cole Swindell
Harry Styles
Post Malone Featuring Doja Cat
Quavo & Takeoff
Jordan Davis
Taylor Swift
Luke Combs
Zach Bryan
Taylor Swift
Lizzo
Drake
Bailey Zimmerman
Drake
Drake
Chris Brown
Drake & 21 Savage
Drake & 21 Savage
Jackson Dean
Drake & 21 Savage
Nicki Minaj
Taylor Swift Featuring Lana Del Rey
Stephen Sanchez
Jax
d4vd
Lil Baby
HARDY Featuring Lainey Wilson
Drake & 21 Savage Featuring Travis Scott
Drake & 21 Savage
Bad Bunny
Ingrid Andress With Sam Hunt
Drake
David Guetta & Bebe Rexha
21 Savage
OneRepublic
Lil Uzi Vert
Lizzo
Jelly Roll
Taylor Swift
Taylor Swift
Elton John & Britney Spears
Armani White
Nate Smith
Taylor Swift
Taylor Swift
Morgan Wallen
Meghan Trainor
Taylor Swift
Taylor Swift
Diddy & Bryson Tiller
DJ Khaled Featuring Drake & Lil Baby
Taylor Swift
Bailey Zimmerman
Selena Gomez
Drake & 21 Savage
Drake Featuring 21 Savage
The Weeknd
Sia
Joji
JVKE
Drake & 21 Savage
Drake & 21 Savage
Rihanna
Future Featuring Drake & Tems
GloRilla & Cardi B
Thomas Rhett Featuring Riley Green
Taylor Swift
Gabby Barrett
Juice WRLD
Luke Bryan
Manuel Turizo
Taylor Swift
Drake & 21 Savage
Steve Lacy
Harry Styles
Morgan Wallen
Taylor Swift
Kane Brown With Katelyn Brown
Taylor Swift
Tems
Rema & Selena Gomez
Burna Boy
Lil Baby
Drake & 21 Savage
Tyler Hubbard
Grupo Frontera
Brent Faiyaz
Oliver Tree & Robin Schulz
Nicky Youre & dazy
Beyonce
Lil Baby Featuring Fridayy
Sam Smith & Kim Petras
Taylor Swift
Drake & 21 Savage
Doja Cat
Taylor Swift
SZA
Taylor Swift
Lil Baby